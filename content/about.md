+++
comments = false
date = "2015-04-14T22:17:00+00:00"
draft = false
noauthor = true
share = false
title = "About me"
type = "page"
[menu.main]
weight = 111

+++
I aim to do the best I can to positively change the world with the knowledge I've acquired and the experiences I've been fortunate enough to earn.   

I love software. Software can be an alleviator of stress, a problem solver, a conduit of information, and a tool to develop deeper insights about the world around us.

I have experience with multiple programming languages, platforms, and operating systems. I've recently been focused on cloud-based solutions and scalable, distributed systems. I work in open source software, and I've spent a considerable amount of time solving challenges in the enterprise.

I have a strong focus on opening information channels via Web APIs, and I'm currently branching out to explore the benefits of connecting physical devices to the digital universe, a concept often identified as the Internet of Things.

 