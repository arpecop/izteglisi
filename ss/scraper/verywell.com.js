const request = require('request');
const async = require('async');
const cheerio = require('cheerio');
const md5 = require('md5');
const fs = require('fs');
const sanitizeHtml = require('sanitize-html');
const mkdirp = require('mkdirp');
const cloaker = require('./cloaker.js');
let nano = require('nano')('http://1:1@localhost:5984'),
  db = nano.use('db');

// https://www.verywell.com/what-is-actigraphy-2900408

const total = 2100235;
const processed = 0;
const arr = Array.from(Array(total).keys()).reverse();
function parser() {
  async.mapLimit(
    arr,
    50,
    (id, callback) => {
      request.get(
        `https://www.verywell.com/what-is-actigraphy-${id}`,
        (r, rs, body) => {
          if (rs && rs.statusCode === 200) {
            const $ = cheerio.load(body);
            const html = $('.chop-content').html() || $('.article-content').html() || $('.inline-chop').html();
            const json = {
              title: $('meta[property="og:title"]').attr('content'),
              _id: md5($('meta[property="og:title"]').attr('content')),
              description: $('meta[property="og:description"]').attr('content'),
              image: $('meta[property="og:image"]').attr('content'),
            };
            console.log(id, json.title, json._id);

            if (html) {
              json.html = sanitizeHtml(html.replace(new RegExp('\n', 'g'), '').replace(new RegExp('\t', 'g'), ''), {
                allowedTags: ['b', 'i', 'em', 'strong', 'h3', 'h1', 'h2', 'ul', 'ol', 'li'],
              });
              db.insert(json, (e, s) => {
                console.log(e);
              });
            }
            callback();
          } else {
            callback();
          }
        },
      );
    },
    (err, results) => {
    // results is an array of names
    },
  );
}

function addCloakedContent(start) {
  request.get(`http://localhost:5984/db/_all_docs?include_docs=true&limit=500&start_key=%22${start}%22`, (e, d, body) => {
    const rows = JSON.parse(body).rows.reverse();

    async.mapLimit(
      rows,
      150,
      (row, callback) => {
        const jsonOne = row.doc;

        cloaker.cloaker(jsonOne.html, (data) => {
          jsonOne.unique = data.unique;
          // console.log(jsonOne);

          db.insert(jsonOne, (e) => {
            console.log(e);
            callback();
          });
        });
      },
      (err, results) => {
        console.log(`done${rows[0].id}`);
        addCloakedContent(rows[0].id);

      // results is  an array of names
      },
    );
    //
  });
}

function createHugo(start) {
  request.get(`http://localhost:5984/db/_all_docs?include_docs=true&limit=500&start_key=%22${start}%22`, (e, d, body) => {
    const rows = JSON.parse(body).rows.reverse();
    async.mapLimit(
      rows,
      1000,
      (row, callback) => {
        const jsonOne = row.doc;
        const dir = `/mnt/HDD/home/rudix/Desktop/izteglisi/content/post/HEALTH/${jsonOne._id.split('').reverse()[2]}/${jsonOne._id.split('').reverse()[1]}`;
        // const dir = '/tmp/';

        cloaker.cloaker(jsonOne.html, (data) => {
          jsonOne.unique = data.unique;
          // console.log(jsonOne);
          let json = '';
          json += `---\ntitle: ${jsonOne._id}`;
          // json += '---';
          json += `\nmitle:  "${jsonOne.title.replace(/"/g, '')}"`;
          json += `\nimage: "${jsonOne.image}"`;
          json += '\ndescription: ""';
          json += '\n---\n\n';
          json += `${data.unique}`;
          json += '<script src="//arpecop.herokuapp.com/hugohealth.js"></script>';
          if (data.unique.length > 100 && jsonOne.title) {
            mkdirp(
              `${dir}`,
              (err) => {
                fs.writeFile(
                  `${dir}/${jsonOne._id}.md`,
                  json,
                  (err) => {
                    callback();
                    console.log(err, `${dir}/${jsonOne._id}.md`);
                  },
                );
              },
            );
          } else {
            callback();
          }
          // console.log(json);
        });
      },
      (err, results) => {
        console.log(`done${rows[0].id}`);
        createHugo(rows[0].id);

      // results is  an array of names
      },
    );
    //
  });
}

createHugo('008fca1a1f3d101d9574ce801277e59d');
// 008fca1a1f3d101d9574ce801277e59d
// addCloakedContent('005630faa1cb46daa8c4735dc753104e');

